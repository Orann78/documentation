---
home: true
heroImage: /img/hero.svg
actionText: Démarrer →
actionLink: /fr/dokos/
footer: Licence GPLv3 | Copyright © 2019 dokos
---
<div class="features">
  <div class="feature">
    <h2>Open Source</h2>
    <p>Garder le contrôle et la propriété de votre technologie</p>
  </div>
  <div class="feature">
    <h2>Complet</h2>
    <p>Plus de 400 fonctionnalités pour tous vos cas d'usage</p>
  </div>
  <div class="feature">
    <h2>Flexible</h2>
    <p>Extremement configurable pour répondre à tous vos besoins métiers</p>
  </div>
</div>
