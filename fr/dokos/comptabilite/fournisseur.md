# Fournisseur

Un **fournisseur** est une personne ou une entreprise qui soit fabrique, transforme, emballe, ou installe des produits contrôlés, soit exerce des activités d'importation ou de vente de ces produits.
On parle souvent de relation client-fournisseur.


### Ajouter un fournisseur

1. Allez à : **Comptabilité > Comptes créditeurs > Fournisseur > Nouveau·elle.(en haut à droite)**
2. Entrez le nom et choisissez la catégorie de votre fournisseur (onglet groupe de fournisseur) ainsi que le type de fournisseur.  
3. Vous pouvez également choisir de mettre son statut sur “désactivé” .

![Nouveau fournisseur](.vuepress/public/images/accounting/supplier/nouveau-fournisseur.png)
