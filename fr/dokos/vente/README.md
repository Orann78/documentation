# Table des matières

- [Groupe de clients](/fr/dokos/groupe-client.md)
- [Client](/fr/dokos/vente/client.md)
- [Devis](/fr/dokos/vente/devis.md)
- [Modèles de taxes](/fr/dokos/vente/modele-taxe.md)
- [Prospect](/fr/dokos/vente/prospect.md)
- [Règles de livraison](/fr/dokos/vente/regle-livraison.md)
