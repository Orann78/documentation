## Client

# Création d'un nouveau client

* Pour créer un nouveau client, cliquez sur l’onglet compatibilité   

* Cliquez ensuite sur client

Vous pouvez maintenant accéder à la liste des clients.
* Pour créer un nouveau client, cliquez sur :

![Nouveau client](.vuepress/public/images/sale/customer/creer-nouveau-client.jpg)

* Commencez par renseigner les informations concernant le client :
    - Le nom du client
    - Le type de client (société ou individuel)
    - Le groupe auquel appartient le client : Vous pouvez créer un nouveau groupe en utilisant le menu déroulant.
    ![Type de clients](.vuepress/public/images/sale/customer/type-client.png)
    - La région du client

    ![Information client](.vuepress/public/images/sale/customer/details-client.jpg)

* Renseigner ensuite les détails du contact principal (client), les détails de l’adresse principale
![Détails du client](.vuepress/public/images/sale/customer/details-client-2.jpg)
* Pour terminer l’enregistrement du client, cliquez sur enregistrer


# Supprimer un client

* Pour supprimer un client allez dans *Compatibilité*   

* Cliquez ensuite sur *client*

* Cochez la case à gauche du nom du client :
![nom_client](.vuepress/public/images/sale/customer/ligne-nom-client.jpg)
* Cliquez sur *Actions* dans le menu en haut à droite :
![bouton-action](.vuepress/public/images/sale/customer/bouton-action.jpg)
* Cliquez dans le menu déroulant sur *Supprimer* :
![menu-actions](.vuepress/public/images/sale/customer/menu-actions.png)




# Paramétrer un client

* Pour paramétrer un client allez dans *Compatibilité*   

* Cliquez ensuite sur le client

* Cliquez ensuite sur le nom du client

* Vous avez accès à la page ci-dessous dans laquelle vous pouvez modifier les paramètres du client : 

![Paramètres du client](.vuepress/public/images/sale/customer/parametres-client.jpg)
![Paramètres du client](.vuepress/public/images/sale/customer/parametres-client-2.jpg)
![Paramètres du client](.vuepress/public/images/sale/customer/parametres-client-3.jpg)

* Pour renommer le client, cliquez sur le nom du client en haut à gauche de la page.

