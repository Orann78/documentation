# Créer un nouvel employé

* Aller dans l’onglet Ressources humaines 

* Cliquez sur "employé" dans l’onglet employé

* Cliquez sur créer un/une nouveau-elle employé

* Renseignez les champs correspondants à la page ci-dessous :

![nouvel-employe](.vuepress/public/images/fr/ressources-humaines/employe/nouvel-employe.jpg)

* Cliquez ensuite sur le bouton *Enregistrer*

# Modifier les données d'un employé

Pour modifier les données de l’employé, cliquez sur le nom de l’employé (voir page ci-dessous) :
![modifier-employe](.vuepress/public/images/fr/ressources-humaines/employe/modif-employe.jpg)

Vous pouvez modifier les données supplémentaires suivantes:
- CONTACT EN CAS D'URGENCE
- UTILISATEUR ERPNEXT
- DÉTAILS D'EMBAUCHE
- DÉPARTEMENT ET ÉCHELON
- DÉTAILS DES PRÉSENCES ET CONGÉS
- DÉTAILS DU SALAIRE
- ASSURANCE SANTÉ
- COORDONNÉES
- BIOGRAPHIE
- DONNÉES PERSONNELLES
- QUALIFICATION POUR L'ÉDUCATION
- EXPÉRIENCE DE TRAVAIL ANTÉRIEURE
- ANCIENNETÉ DANS LA SOCIÉTÉ

