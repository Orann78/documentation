## Bienvenue sur le site de la documentation de dokos

dokos est une adaptation d'un logiciel de gestion open-source bien connu: ERPNext.  
Bénéficiant de plus de 10 ans de développements et d'une utilisation dans des milliers d'entreprises, c'est un logiciel de gestion robuste et performant.

dokos a été créé pour permettre d'adapter ce logiciel aux normes françaises et européennes et d'accélérer son développement en Europe.

Il est distribué sous licence GPLv3.

---

La documentation de dokos est en cours de création et vous pouvez participer à son amélioration en cliquant sur le lien en bas de chaque page.

Pour naviguer dans la documentation, vous pouvez utiliser le champ de recherche dans la barre de navigation ou faire une recherche par tag dans la page [Tags](/tags).


## Liens rapides

### Général

- [Démarrer avec dokos](/fr/dokos/installation/)

### Modules

- [Comptabilité](/fr/dokos/comptabilite/)
- [Site web](/fr/dokos/site-web/)
- [Stocks](/fr/dokos/stocks/)
- [Ventes](/fr/dokos/ventes/)
- [Ressources humaines](/fr/dokos/ressources-humaines/)

---
Une partie du contenu est une adaptation de la [documentation d'ERPNext](https://erpnext.com)  
Licence: [CC 4.0 BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/)