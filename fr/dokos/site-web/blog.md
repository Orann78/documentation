# Création d'un blog

* Pour créer un nouveau blog, cliquez sur l’onglet site web
* Cliquez ensuite sur créer une nouveau-elle "Article de blog"
* Vous arrivez ensuite sur la page ci-dessous dans laquelle vous devez renseigner les champs :
    - Titre
    - Catégorie du Blog
    - Publié le
    - Blogueur
    - Route (chemin dans l'arborescence du site)
    
![Créer un nouveau blog](.vuepress/public/images/website/blog_article/creer-blog.jpg)

* Après avoir renseigné tous les champs, vous pouvez commencer la rédaction de votre blog. Dans l’onglet introduction, rédiger l’introduction de votre blog qui sera placé juste en-dessous de votre titre. Dans l’onglet contenu, vous pouvez rédiger un descriptif, insérer du contenu multimédia (lien, image, vidéos). Voir image ci-dessous :

![Contenu du blog](.vuepress/public/images/website/blog_article/contenu-blog.jpg)