# Page web

Définissez ici les différentes **pages web** qui seront accessibles depuis votre site Internet.
 
### Créer une nouvelle page web

1. Allez à : **Site web > Site Internet > Page Web > Nouveau·elle.(en haut à droite)**
2. Entrez le titre de votre page web et remplissez son chemin dans l’arborescence de votre site.  
3. Si vous avez préalablement créé un diaporama (voir documentation associée), vous pouvez en choisir un pour votre page web.
4. Dans la partie contenu, choisissez le format qui vous convient pour écrire votre page. Pour les débutants, “text riche” est le plus intuitif.
5. Vous pouvez activer l’option “modèle dynamique” pour utiliser des scripts javascript. Vous pouvez écrire votre code dans l’onglet adapté.
6. L’onglet “Style” permet de gérer la mise en page du texte. Vous pouvez importer votre propre page de style en langage CSS.
7. Une fois votre page créée, vous aurez accès à de nouveaux onglets concernant les méta-données ou la personnalisation des en-têtes.
