# Comment contribuer

- Répondez à des questions sur le [Forum](https://community.dokos.io)
- Notifiez l'équipe de l'existence de bugs sur [Gitlab](https://gitlab.com/dokos/dokos/issues)
- [Traduisez](/fr/dokos/contribuer/traductions) le logiciel dans votre langue