# Translations

All text strings in __dokos__ are translatable.  
If you find any untranslatable string, please open an issue on [Gitlab](https://gitlab.com/dokos/dokos/issues).

## Translation logic

__dokos__ is composed of several levels of translatable data. Some translatables strings are available in Python or Javascript files and some data registered in the database need also to be translated in the user interface to be readable by all users.

The main categories of translatable data are:
- Strings composing a doctype
- Strings in Python files
- Strings in Javascript files
- Strings in HTML files
- Strings in VueJs files

In order to make contextual translations available for languages having several meaning for the same word, we have decided to allow different translations for each of these categories and, in the case of code files, for each file composing the system.

These translations are available in JSON files, located in `{app name}/translations/`

Each translatable file is listed in the translation JSON file:  
![Translatable files](/images/contributing/translations/translatable_files.png)

With its corresponding translatable strings:  
![Translatable strings](/images/contributing/translations/translatable_strings.png)


## Correct translated strings

While using __dokos__ you may notice that one piece of text is incorrectly translated in your language.  
You have two main options:

- Create an issue on [Gitlab](https://gitlab.com/dokos/dokos/issues) describing the word or sentence incorrectly translated and providing the correct translation.
  Please be very specific (a screenshot is always welcome) and keep in mind that our team may not speak your language and may have difficulties making the corrections if your input is not specific enough. 

- Go to [dokos source code](https://gitlab.com/dokos/dokos/tree/develop/erpnext/translations) or [dodock source code](https://gitlab.com/dokos/dodock/tree/develop/frappe/translations).
  Find the file corresponding to your language, edit it and submit a merge request.
  You can also clone the repository to work in your favorite text editor before submitting a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/).
  This option is the recommended one if you plan on editing a lot of translations at once.


## Translate new strings

If you want to update strings corresponding to the latest update in __dokos__, you can also query all untranslated strings, translate them and push them back to be merged with the existing translations:

### Step 1 - Get all untranslated strings

`bench get-untranslated {language code} {path to a temporary file}.json --app {the application you wish to translate}`

E.g. `bench get-untranslated fr /home/dokos_fr.json --app dokos`


### Step 2 - Make your translation

Translate all strings in the generated JSON files.


### Step 3 - Update existing translations 

Merge your new translations with the existing translations using the following command:

`bench update-translations {language code} {path to a temporary file}.json {the application you wish to translate}`

E.g. `bench update-translations fr /home/dokos_fr.json dokos`


### Step 4 - Migrate your application to finalize the update

In order to see the translated strings in your instance, launch:

`bench migrate`

### Step 5 - Send a merge request

Once you have finalized your translations, don't forget to contribute back by opening a merge request.  
It takes only a few minutes, but helps improve the software for everyone hugely !


## Adding a new language

In order to add a new language to dokos, you can use the following command:

`bench new-language {language code} {application}`

E.g. `bench new-language de dokos`

A new file containing all translatable strings will be created in the `translations` folder.
