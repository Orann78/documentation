---
tags: ["email"]
---
# Email setup

dokos has an embedded sending and receiving email system.
You can configure default sending and receiving accounts, as well as accounts for each of your users.

## 1. Email domain

In order to setup your email account, you need to start by registering the parameters to connect to your email provider.
If several users have a common email domain name, it is recommanded to create a email domain in dokos to make this setup only once.

Go to `Settings > Email Domain` and create a new settings document.

### 1.1. General

- **Example Email Address**
  Register an example corresponding to your email domain. E.g. `hello@dokos.io`

### 1.1. Incoming Emails

- Email Server
  Register the paramaters provided by your email supplier or your email server hosting service
  E.g. `imap.gmail.com`, `SSL0.OVH.NET`,...

- If the above parameters correspond to the IMAP protocol, check **Use IMAP**

- If you want to use the SSL protocol to receive your emails, check **Use SSL**

- Choose the port to use to receive your emails.
  If your don't fill this field, dokos will use the standard ports defined in the [imaplib](https://docs.python.org/3.6/library/imaplib.html) library if your use IMAP or [poplib](https://docs.python.org/3.6/library/poplib.html) library else.

- Define a maximum size for attachments
  > Old API currently not used - will be deprecated

### 1.2. Outgoing Emails

- **SMTP Server**
  Register the parameters provided by your email supplier or your email server hosting service
  E.g. `smtp.gmail.com`, `SSL0.OVH.NET`,...

- If you want to use the SSL protocol to send your emails, check **Use TLS**

- Choose the port to use to send your emails.
  If you don't fill this field, dokos will use port 587.


## 2. Email account

You can create one or several email accounts for your and your collaborators.
If you want dokos to automatically send emails (notifications, weekly reports, ...) you will need at least a default sending account.

### 2.1. New email account

- **Email Address**: Register the email address of the account you want to add in dokos.
- **Use Different Email Login ID**: If the email address to connect to your account is not the one registered in the preceding field, define it in the field **Email Login ID**.
  E.g. Your address is `john@mycompany.com` but your connection ID is `john123456`. 
- **Password**: Your email account's password.
- **Awaiting password**: If you create an email account for your collaborators without knowing their password, check this checkbox.  
  After their next login to dokos, a popup will ask them to define their password in order to allow their email account to connect to your email server.
- **Use ASCII encoding for password**: To be checked if your email provider requires ASCII encoded passwords.
- **Email Account Name**: Choose a name for this email account. E.g. `Notifications` or `John Doe`
- **Domain** or **Service**: If you have configured an email domain, select it here, else you can select one of the proposed services or leave these two fields empty to manually configure your email account access parameters.

### 2.2. Receiving emails

- **Enable Incoming**: check this checkbox if you want to receive emails from this email account.
- For access settings elements, please see the domain email section (**Use IMAP**, **Email Server**, **Use SSL**, **Port**, **Attachment Limit (MB)**)
- **Append To**: All emails sent to this address will be directly linked to an existing document or to a new document from this document type.
  It can be useful, for example, to create a new opportunity each time someone sends an email to an address like `sales@mycompany.com`.
  You can add new document types via the `email_append_to` hook in a custom application.
- **Default Incoming**: Setup this address as a the generic reply-to address (`reply-to` field in emails) if the sending account cannot receive emails.
- **Email Sync Option**: Choose between `ALL` and `UNSEEN` for the first synchronization of your email account.
  `ALL` will fetch the x last emails from your inbox, while `UNSEEN` will fetch the x last unread emails from your inbox.
- **Initial Sync Count**: Select the number of emails to fetch during the intial synchronization.

### 2.3. Automatic linking

After activation of this option, dokos will generate a specific email address for all documents.

![automatic link](/images/setup/email_configuration/automatic_link.png)

If you send an email to this address, it will automatically be linked to the corresponding document.

This option only works with one email account.

### 2.4. Notifications if unreplied

You can configure the mailing of a notification if you don't receive a response to your initial email after x minutes.

- Check **Notify if unreplied**
- Define a period before triggering the notification (in minutes)

:::tip Tip
If you want to define a period of several days, use the calculator integrated in dokos' fields.
E.g. For 2 days, write `60*24*2` and dokos will be able to calculate the number of minutes.
:::

- **Send Notification to**: Add the email addresses of the relevant people

### 2.5. Sending Emails

- If you want to send emails with this email account, check **Enable Outgoing**
- For access settings elements, please see the domain email section (**SMTP Server**, **Use TLS**, **Port**)
- **Default Outgoing**: By checking the checkbox, you define this email account as the default sending email account for your dokos instance. All emails sent automatically by the system or by users without an attributed email account will go through this email account.
- **Always use Account's Email Address as Sender**: When your emails are sent, the email address of this account is used as sender name. It is the recommended option when your name is composed of special characters to avoid been sent to the spam folder of the recipient.
- **Always use Account's Name as Sender's Name**: When your emails are sent, the name of this email account is used as sender name. This option takes precedence over the previous one.
- **Send unsubscribe message in email**: Adds a link `Leave this conversation` at the bottom of each email
- **Track Email Status**: If you check this checkbox, dokos will notify you (through the email status) if it read by the recipient. If you send it to several recipients, it will be considered as read if 1 recipient reads it.
- **Disable SMTP server authentication**: Option for SMTP servers without authentication.

### 2.6. Signature

This signature will be added above the footer and under the individual signature of the user sending the email.

:::tip Good to know
The signature of an email can be composed of two levels in dokos:
- The user signature - `Users and permissions > User`
- The email account signature - `Settings > Email account`
:::

### 2.7. Automatic reply

You can activate this option to send a new automatic reply when you receive an email.
You can format your message with variables by using the <a href="https://palletsprojects.com/p/jinja/" target="_blank">Jinja</a> language.
The reference document type is a `Communication`.

### 2.8. Footer

You can define a footer to be added above the global footer defined in System Settings.

:::tip Good to know
The footer of an email can be composed of three levels in dokos:
- The email account's footer - `Settings > Email Account`
- The global footer for the instance - `Settings > System Settings`
- dokos standard footer - `Settings > System Settings`
:::
