# Print formats

dokos gives the possibility to completely customize your print formats for all document types.  
You can use pre-configured print styles and settings or completely customize your print formats directly from the user interface.

## 1. Configure a print format

In order to configure your print formats, dokos provides two levels of settings:
1. Print settings
  These settings are global and affect all print formats
2. Print format
  Each print format has its own set of settings, applicable only to itself.

## 2. Print settings

In order to configure your print settings, go to `Settings > Print Format`.

### 2.1. PDF settings

- **Send Print as PDF**: It will send all document print attached to an email as a PDF file.
  If not set, all document prints will be sent as HTML files.
- **Repeat Header and Footer in PDF**: The header and footer of the PDF will be repeated on all pages.
  If not set, the header will be only added to the first page and the footer will not be displayed.
- **PDF Page Size**: Officially supported page sizes are `A4` and `Letter`.

:::tip Pro Tip
If needed you can customize the page size field to add other options supported by [wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf/blob/master/src/lib/pdfsettings.cc) (The pdf generation library).  
E.g. A0, A1,..., A5, A6,... Folio, Tabloid,...
:::

### 2.2. Page settings

- **Print with letterhead**: Enables the `Letter Head` option of the print preview by default.
  It can be overriden in each document.
- **Compact Item Print**: Enables a more compact print format for item tables.
- **Allow Print for Draft**: Enables the print view for draft documents.
- **Send document web view link in email**: If set, it will add a link to view an HTML version of the document print in the recipients's browser.
- **Always add "Draft" Heading for printing draft documents**: Adds the word `DRAFT` at the top of every printed draft document.
- **Allow page break inside tables**: If enable, users will be able to create page break between items by checking a checkbox inside each item line.
- **Allow Print for Cancelled**: Enables the print view for cancelled documents.
- **Print taxes with zero amount**: Will print tax lines with a value equal to 0. Otherwise they will not be printed.

### 2.3. Print server

You can figure a print server linked to dokos by adding you server IP and connection port.
dokos will fetch all printers linked to this address and you will be able to select the printer to use in order to print documents from the system.

:warning: **You may need to install additional python libraries to your servers**

### 2.4. Print style

A print style is a CSS sheet adding style to your print format. You can use the [Bootstrap 3.3](https://getbootstrap.com/docs/3.3/getting-started/) framework and use its classes and components.

You can choose between several print styles provided by default with dokos or create your own print style.

A print style will be applied by default to all print formats created for your dokos site.
They can be overriden in each print format by specific CSS style.

Therefore, it is recommended to create a basic style that will be applied to all your documents print formats and to only add some specific style for each print format if needed.

Standard print formats can not be modified, because they may be updated with the software over times.
Instead, it is recommended to copy the standard print format you want to modify, to edit it and to select it in your print settings.
You may also disable print styles you do not wish to use.

### 2.5. Fonts

You can choose between several fonts provided by default and adjust the standard font size of your print formats.

## 3. Print format

There are several types of print formats:

1. **Standard**: Are embedded in the code of dokos and may be updated over time. These print formats can be copied to be customized.
2. **Not standard** and not **custom**: Customized print formats modified through the **Print Format Builder**.
3. **Not standard** and **custom**: Customized print formats in [Jinja](https://jinja.palletsprojects.com/en/master/) and generated server side or client side.

A print format can be created and modified through two different channels:
1. Go to `Settings > Print Format` and click on new or select your print format to edit.
2. If your print format is **not standard** and not a **custom** print format, you can go to `Settings > Print format builder` and select your print format or a reference document to create a new one.


### 3.1. Standard print formats

Standard print formats are provided with the software and can be used as is or customized.
In order to customize them, you need to make a copy (`Menu > Duplicate`) and to name your copy differently from standard print format.

Each print format is linked with a module and a reference document type.
You have access to all fields in the reference and your print format will only be fetched in this document type's printview.

When duplicating a standard print format, make sure you keep the same type of format: if `Custom Format` is not checked, you must use the Print Format Builder to customize it. Otherwise you'll end up making an entirely different print format.

### 3.2. User specific print formats

### 3.2.1. Custom format

#### Print format type

A custom format can be of two types:
- JINJA: Will be printed server side
- JS: Will be printed in the web browser


You can build your print format in HTML using the [Jinja](https://jinja.palletsprojects.com/en/master/) language and add style in CSS using [Bootstrap 3.3](https://getbootstrap.com/docs/3.3/getting-started/) classes and components.

If your printer allows raw printing, you can provide printer specific commands by checking **Raw Printing**.


### 3.2.2. Print Format Builder generated format

If you prefer to use the print format builder, you can define a few additional settings specific for your print format:
- **Align Labels to the Right**: Will align all labels to the right. Otherwise they will be aligned left by default.
- **Show Section Headings**: Will display the section header in the print format. This header can be customized in the print format builder.
- **Show Line Breaks after Sections**: Will add line breaks after each section
- **Default Print Language**: Will be selected by default when printing a document
- **Font**: Will override the global font defined in print settings

You can also add some CSS style in order to override the default style of your print format.

#### Edit format

You can then click on `Edit Format` to start editing your print format.

The print format builder will give you the possibility to add sections and columns as well as edit the heading of your print format.

#### Sections and columns

You can add as many sections as you want and up to 4 columns per section.
Just click on the wheel at the top of each section to configure it:
- **N° of columns**: Add up to 4 columns
- **Section heading**: Edit the title of the section. It will only be displayed if the setting is enabled in the print format properties (see above)
- **Page Break**: Will add a page break just below this section.

You can remove a section by clicking on the `remove` button.

#### Heading

You can also edit the Heading displayed at the top of the first page of your document.
Just change the labels or customize your heading completely if you feel comfortable with HTML and/or Jinja.

#### Fields

Once your layout is ready, just drag and drop fields in each columns to display them on your print format.
If the field has no value it will not be displayed.

By default, you can add all fields available for printing in the reference document.
If you want to fetch a value from another document linked to your reference document, you can add a `Custom HTML` field and use Jinja code.

E.g. You want to fetch the default bank account of a customer to print it on his/her invoice:
```
{% set bank_account = frappe.db.get_value("Customer", doc.customer, "default_bank_account") %}
<div class="row">
	<div class="col-md-3 text-right">{{ _("Bank Account") }}</div>
	<div class="col-md-9">{{ bank_account or '' }}</div>
</div>
```

#### Translations

If you add translatable labels to your code, don't forget to also add translations for each language you plan to use in the `Translation` document type.

### 3.3. Several print formats

If you have several print formats for one document, click on `Make Default` to define the most used format as default for your document.