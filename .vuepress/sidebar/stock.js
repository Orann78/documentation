module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				children: [
					'',
					'reservation-articles'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			children: [
				'',
				'item-booking'
			]
		}
	]
}