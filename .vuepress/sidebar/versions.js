module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				children: [
					'',
					'v1_0_0',
					'v1_1_0'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			children: [
				'',
				'v1_0_0',
				'v1_1_0'
			]
		}
	]
}