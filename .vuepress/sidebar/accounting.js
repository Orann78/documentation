module.exports = function getSidebar (name, lang) {
	if (lang === 'fr') {
		return [
			{
				title: name,
				collapsable: false,
				children: [
					'',
					'facture-vente',
					'demande-paiement',
					'prelevement-sepa',
					'passerelles-paiement'
				]
			}
		]
	}

	return [
		{
			title: name,
			collapsable: false,
			children: [
				'',
				'sales-invoice',
				'payment-request',
				'sepa-direct-debit',
				'payment-gateways'
			]
		}
	]
}